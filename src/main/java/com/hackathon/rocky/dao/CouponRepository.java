package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface CouponRepository extends JpaRepository<Coupon,String> {

}
