package com.hackathon.rocky.dao;

import com.hackathon.rocky.entity.ContractTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface Contract_TransactionRepository extends JpaRepository<ContractTransaction,String> {

}
