package com.hackathon.rocky.controller;

import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/check")
public class UserController {
    @Autowired
    private UserService userService;


    @GetMapping("/save")
    public ResponseEntity<?> saveUser(){
        User user1 = new User();
        user1.setUser_name("Admin");
        user1.setUser_point(200);

        // save contract data
        userService.saveUser(user1);

        return new ResponseEntity(user1, HttpStatus.OK);
    }


}
