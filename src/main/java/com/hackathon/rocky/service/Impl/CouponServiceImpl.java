package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.entity.Coupon;
import com.hackathon.rocky.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CouponServiceImpl implements CouponService {

    @Autowired
    private CouponRepository couponRepository;

    public CouponServiceImpl(CouponRepository CouponRepository){this.couponRepository = CouponRepository;}



    public void saveCoupon(Coupon coupon) {

        couponRepository.save(coupon);
    }

    @Override
    public void updateCoupon(Coupon coupon) {
        couponRepository.save(coupon);
    }

    @Override
    public void deleteCoupon(Coupon coupon) {
        couponRepository.delete(coupon);
    }
}
