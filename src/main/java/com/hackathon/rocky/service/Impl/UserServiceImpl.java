package com.hackathon.rocky.service.Impl;

import com.hackathon.rocky.dao.CouponRepository;
import com.hackathon.rocky.dao.UserRepository;
import com.hackathon.rocky.entity.User;
import com.hackathon.rocky.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository){this.userRepository = userRepository;}

    public void saveUser(User user){
        userRepository.save(user);
    }
}
