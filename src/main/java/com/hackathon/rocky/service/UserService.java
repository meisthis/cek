package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.User;
import org.springframework.stereotype.Service;


public interface
UserService  {

    void saveUser(User user);
}
