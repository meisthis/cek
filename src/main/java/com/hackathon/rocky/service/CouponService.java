package com.hackathon.rocky.service;

import com.hackathon.rocky.entity.Coupon;

public interface CouponService {

    void saveCoupon(Coupon coupon);
    void updateCoupon(Coupon coupon);
    void deleteCoupon(Coupon coupon);

}
