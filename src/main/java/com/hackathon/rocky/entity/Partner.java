package com.hackathon.rocky.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@Table(name = "Transaction")
@DynamicUpdate
public class Partner implements Serializable {


    @Id
    @Column(name = "Transaction_id")
    private String transaction_id;
}

