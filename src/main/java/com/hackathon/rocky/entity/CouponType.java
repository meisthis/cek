package com.hackathon.rocky.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "Coupon_Type")
@DynamicUpdate
public class CouponType  {

    @Id
    @Column(name="Coupon_type_id")
    private String coupon_type_id;

    @Column(name="Coupon_type_name")
    private String coupon_type_name;
}
