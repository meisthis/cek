package com.hackathon.rocky.entity;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@Table(name = "Coupon")
@DynamicUpdate
public class Coupon implements Serializable {

    @Id
    @Column(name = "coupon_id")
    private String coupon_id;

    @Column(name = "coupon_name")
    private String coupon_name;

    @Column(name = "coupon_picture")
    private String coupon_picture;

    @Column(name = "coupon_desc")
    private String coupon_desc;



}
