package com.hackathon.rocky.entity;


import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.GenericGenerators;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "Contract_Transaction")
@DynamicUpdate
public class ContractTransaction {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy="uuid")
    @Column(name = "Contract_Transaction_id")
    private String contract_transaction_id;

    @Column(name = "Total_Cicilan")
    private BigInteger total_cicilan;

    @Column(name = "Tenor_Price")
    private BigInteger tenor_price;

}
